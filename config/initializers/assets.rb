Rails.application.config.assets.precompile += %w(
    froala/froala_editor.min.js
    froala/plugins/*.js
    froala/languages/*.js
    froala/froala_editor.min.css
    froala/froala_style.min.css
    froala/plugins/*.css
    froala/themes/*.css
)